package com.phamcongtuan.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.phamcongtuan.model.VideoYoutube;
import com.phamcongtuan.youtubeplaylistplayer.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Admin on 9/5/2017.
 */

public class VideoYoutubeAdapter extends ArrayAdapter<VideoYoutube> {
    Activity context;
    int resource;
    List<VideoYoutube> objects;

    public VideoYoutubeAdapter(Activity context, int resource, List<VideoYoutube> objects) {
        super(context, resource, objects);
        this.context=context;
        this.resource=resource;
        this.objects=objects;
    }

    public void AddListItemToAdapter(List<VideoYoutube> list) {
        objects.addAll(list);
        this.notifyDataSetChanged();
    }

    private class ViewHolder{
        ImageView imgThumbnail;
        TextView txtTitle;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;

        if (view == null){
            holder = new ViewHolder();
            LayoutInflater inflater = this.context.getLayoutInflater();
            view = inflater.inflate(this.resource,null);
            holder.imgThumbnail = (ImageView) view.findViewById(R.id.imgThumbnail);
            holder.txtTitle = (TextView) view.findViewById(R.id.txtTitle);
            view.setTag(holder);
        }
        else {
            holder = (ViewHolder) view.getTag();
        }

        VideoYoutube video = this.objects.get(position);
        holder.txtTitle.setText(video.getTitle());
        Picasso.with(this.context).load(video.getUrlImage()).into(holder.imgThumbnail);

        return view;
    }
}
