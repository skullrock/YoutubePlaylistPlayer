package com.phamcongtuan.model;

/**
 * Created by Admin on 9/5/2017.
 */

public class VideoYoutube {
    String title;
    String urlImage;
    String videoId;

    public VideoYoutube() {
    }

    public VideoYoutube(String title, String urlImage, String videoId) {
        this.title = title;
        this.urlImage = urlImage;
        this.videoId = videoId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

}
