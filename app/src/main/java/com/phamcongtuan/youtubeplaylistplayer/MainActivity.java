package com.phamcongtuan.youtubeplaylistplayer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.phamcongtuan.adapter.VideoYoutubeAdapter;
import com.phamcongtuan.model.VideoYoutube;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static String API_KEY = "AIzaSyAVorRoghW0h523ohTHFVBfslNpb4PLV-E";
    String ID_PLAYLIST = "PLUFl2i5Znh_Y5-U9TXz552SNamNR2tZjN";
    String urlGetJson = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId="+ID_PLAYLIST+"&key="+API_KEY+"&maxResults=50";

    ListView lvVideoYoutube;
    VideoYoutubeAdapter adapterVideoYoutube;
    ArrayList<VideoYoutube> arrVideo;
    ArrayList<VideoYoutube> arrVideoPlus;

    int current =0;
    int INIT_ITEM = 10;
    int ITEM_ADD = 10;

    Handler mHandler;
    View ftView;
    boolean isLoading = false;

    BroadcastReceiver langNgheInternet = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
            if (connectivityManager.getActiveNetworkInfo() == null) {
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
                alertDialog.setTitle("Alert !!!");
                alertDialog.setMessage("Please turn on Wifi ...");
                alertDialog.setPositiveButton("RESTART", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(MainActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                });
                alertDialog.show();
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(langNgheInternet,intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(langNgheInternet);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addControls();
        addEvents();
    }

    private void addEvents() {
        GetJsonYoutubePlaylist(urlGetJson);

        lvVideoYoutube.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, PlayVideoActivity.class);
                intent.putExtra("idVideo", arrVideo.get(position).getVideoId());
                startActivity(intent);
            }
        });

        lvVideoYoutube.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (view.getLastVisiblePosition() == totalItemCount-1 && lvVideoYoutube.getCount() >= arrVideo.size()
                        && isLoading == false
                        //Neu da load het thi khong hien load nua
                        && lvVideoYoutube.getCount()<arrVideoPlus.size()+INIT_ITEM) {
                    isLoading = true;
                    Thread thread = new ThreadGetMoreData();
                    thread.start();
                }
            }
        });

    }

    private void addControls() {
        lvVideoYoutube = (ListView) findViewById(R.id.lvVideoYoutube);

        LayoutInflater inflater = this.getLayoutInflater();
        ftView = inflater.inflate(R.layout.footer_view, null);
        mHandler = new MyHandler();

        arrVideo = new ArrayList<>();
        arrVideoPlus = new ArrayList<>();
        adapterVideoYoutube = new VideoYoutubeAdapter(this,R.layout.item_video,arrVideo);
        //Android duoi 4.4 thi phai addFooterView truoc khi setAdapter, Android 4.4 tro di khong can
        lvVideoYoutube.addFooterView(ftView);
        lvVideoYoutube.setAdapter(adapterVideoYoutube);
        lvVideoYoutube.removeFooterView(ftView);
    }

    private void GetJsonYoutubePlaylist(final String url){
        final RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            JSONArray jsonItems = response.getJSONArray("items");
                            String title = "";
                            String urlImage = "";
                            String videoId = "";
                            for (int i=0; i<jsonItems.length(); i++){
                                JSONObject jsonItem = jsonItems.getJSONObject(i);
                                JSONObject jsonSnippet = jsonItem.getJSONObject("snippet");
                                title = jsonSnippet.getString("title");
                                JSONObject jsonThumbnail = jsonSnippet.getJSONObject("thumbnails");
                                JSONObject jsonMedium = jsonThumbnail.getJSONObject("medium");
                                urlImage = jsonMedium.getString("url");
                                JSONObject jsonResourceId = jsonSnippet.getJSONObject("resourceId");
                                videoId = jsonResourceId.getString("videoId");

                                VideoYoutube video = new VideoYoutube(title, urlImage, videoId);
                                //Lay so luong item hien ra lan dau && response.getString("prevPageToken")==null
                                if (i<INIT_ITEM && response.isNull("prevPageToken")) {
                                    arrVideo.add(video);
                                }
                                //Chua cac item de load more
                                else {
                                    arrVideoPlus.add(video);
                                }
                            }
                            if (!response.isNull("nextPageToken")){
                                String urlNew = urlGetJson + "&pageToken=" + response.getString("nextPageToken");
                                GetJsonYoutubePlaylist(urlNew);
                            }
                            adapterVideoYoutube.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, "Loi !!!", Toast.LENGTH_SHORT).show();
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);

    }

    public class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case 0:
                    lvVideoYoutube.addFooterView(ftView);
                    break;
                case 1:
                    adapterVideoYoutube.AddListItemToAdapter((ArrayList<VideoYoutube>)msg.obj);
                    lvVideoYoutube.removeFooterView(ftView);
                    isLoading = false;
                    break;
                default:
                    break;
            }
        }
    }

    private ArrayList<VideoYoutube> getMoreData() {
        ArrayList<VideoYoutube> list = new ArrayList<>();
        int temp = current;
        for (int i=temp; i<temp+ITEM_ADD; i++){
            if (i<arrVideoPlus.size()){
                VideoYoutube videoYoutube = arrVideoPlus.get(i);
                list.add(videoYoutube);
                current++;
            }
            else {
                break;
            }
        }
        return list;
    }

    public class ThreadGetMoreData extends Thread {
        @Override
        public void run() {
            mHandler.sendEmptyMessage(0);
            ArrayList<VideoYoutube> listResult = getMoreData();
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Message message = mHandler.obtainMessage(1, listResult);
            mHandler.sendMessage(message);
        }
    }
}
